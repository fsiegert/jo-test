from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->mu+,nu_mu;:OnOff Off
set /Herwig/Particles/W+/W+->e+,nu_e;:OnOff Off
set /Herwig/Particles/W+/W+->tau+,nu_tau;:OnOff Off
set /Herwig/Particles/W+/W+->dbar,u;:OnOff On
set /Herwig/Particles/W+/W+->sbar,c;:OnOff On
set /Herwig/Particles/W+/W+->sbar,u;:OnOff On
set /Herwig/Particles/W+/W+->dbar,c;:OnOff On
set /Herwig/Particles/W+/W+->bbar,c;:OnOff On
##set W- decay
set /Herwig/Particles/W-/W-->d,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->s,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->s,ubar;:OnOff Off
set /Herwig/Particles/W-/W-->d,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->b,cbar;:OnOff Off
set /Herwig/Particles/W-/W-->e-,nu_ebar;:OnOff On
set /Herwig/Particles/W-/W-->mu-,nu_mubar;:OnOff On
set /Herwig/Particles/W-/W-->tau-,nu_taubar;:OnOff On
"""



# LHEHandler generated events/number of attempts ~75%
safefactor = 1.5 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> W+ W-, W+ -> lep, W- -> jj"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "W", "WW"] 
evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"] 
run_number_min = 343712 
run_number_max = 343719
offset = 0

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")




