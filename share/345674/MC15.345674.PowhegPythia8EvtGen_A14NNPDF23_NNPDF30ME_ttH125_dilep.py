#--------------------------------------------------------------
# Powheg ttH setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ttH_Common.py')
PowhegConfig.topdecaymode = 22200 #ttbar we use 22222 which turns on all decay modes
PowhegConfig.hdamp        = 352.5

PowhegConfig.runningscales = 1 ## dynamic scale
PowhegConfig.storeinfo_rwgt = 1
PowhegConfig.PDF = range(260000, 260101) + range(90400, 90433) + [11068] + [25200] + [13165] + [25300] # PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.nEvents *= 1.1 #to compensate for efficiency filter
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttH production with A14 NNPDF2.3 tune'
evgenConfig.keywords    = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact     = [ 'maria.moreno.llacer@cern.ch', 'jannik.geisen@cern.ch' ]
