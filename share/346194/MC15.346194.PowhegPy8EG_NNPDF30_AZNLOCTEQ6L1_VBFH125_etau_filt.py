
evgenConfig.process     = "VBF H->etau"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->etau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.minevents   = 2000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:oneChannel = 1 0.5 100 15 -11',
	                     '25:addChannel = 1 0.5 100 11 -15' ]

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lfvfilter = TauFilter("lfvfilter")
  filtSeq += lfvfilter

filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 1
filtSeq.lfvfilter.Nleptaus = 0
filtSeq.lfvfilter.Nhadtaus = 0
filtSeq.lfvfilter.EtaMaxlep = 2.6
filtSeq.lfvfilter.EtaMaxhad = 2.6
filtSeq.lfvfilter.Ptcutlep = 10000.0 #MeV
filtSeq.lfvfilter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 20000.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 20000.0 #MeV
