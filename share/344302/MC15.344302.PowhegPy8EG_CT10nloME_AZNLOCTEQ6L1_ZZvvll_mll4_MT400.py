#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'ZZvvll'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = 11000  #range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = 1.0 #[ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = 1.0 #[ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *=75
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->vvll production with AZNLO CTEQ6L1 tune and mllmin4'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'monica.trovatelli@cern.ch', 'roberto.di.nardo@cern.ch' ]
evgenConfig.minevents   = 2000

include("MC15JobOptions/TransverseMassVVFilter.py")
filtSeq.TransverseMassVVFilter.MinMass = 400000
filtSeq.TransverseMassVVFilter.MaxMass = 14000000
filtSeq.Expression = "TransverseMassVVFilter"
