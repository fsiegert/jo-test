######################################################################
# Job options for Pythia8B_i generation of Bd -> K*0(K+Pi-)mu3p5mu3p5.
######################################################################

evgenConfig.description = "Signal Bd->Kstar(K+Pi-)mu3p5mu3p5"
evgenConfig.keywords = ["exclusive","Bd","2muon","rareDecay"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 9.' ]
genSeq.Pythia8B.QuarkPtCut      = 0.0
genSeq.Pythia8B.AntiQuarkPtCut  = 7.0
genSeq.Pythia8B.QuarkEtaCut     = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands += [ '511:onIfMatch = 313 -13 13' ]
genSeq.Pythia8B.SignalPDGCodes = [   511,   313,  321, -211,   -13,    13 ]
genSeq.Pythia8B.SignalPtCuts   = [   0.0,   0.0,  0.5,  0.5,   0.0,   0.0 ] # no muon pT cut here - see trigger cuts below
genSeq.Pythia8B.SignalEtaCuts  = [ 102.5, 102.5,  2.6,  2.6, 102.5, 102.5 ] # no muon eta cut here - see trigger cuts below

genSeq.Pythia8B.NHadronizationLoops = 2

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
