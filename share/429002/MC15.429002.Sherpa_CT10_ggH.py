include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa gg->H with H->gammagamma decay. Validation sample."
evgenConfig.keywords = ["SM", "Higgs", "diphoton" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=1,2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR=Comix Amegic Internal MCFM;
  SHERPA_LDADD=SherpaMCFM
  ABS_ERROR=0.07
  ERROR=0.99

  %model setup
  MODEL SM+EHC
  MASS[25]=125.; YUKAWA[5]=0; YUKAWA[15]=0
  MASSIVE[15] 1;

  %decay setup
  HARD_DECAYS On;
  HARD_SPIN_CORRELATIONS=1
  STABLE[25]=0; WIDTH[25]=0.;
  HDH_ONLY_DECAY {25,22,22}
}(run)

(processes){
  Process 93 93 -> 25 93{NJET};
  Order_EW 1; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  Loop_Generator Internal {LJET} {1,2};
  Loop_Generator MCFM {3};
  End process;
}(processes)

(selector){
}(selector)
"""
