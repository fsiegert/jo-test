#######################################################################
# Job options fragment for pp->X X(3872)(->pi,pi,Jpsi(mu4mu4))
#######################################################################
evgenConfig.description = "pp->X Psi(2S)(->pi,pi,Jpsi(mu4mu4))"
evgenConfig.keywords = ["charmonium","pi+","pi-","2muon","Jpsi"]
evgenConfig.minevents = 200

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] # Equivalent of CKIN3

# Turn on colour-octet decays into Psi(2S)
genSeq.Pythia8B.Commands += ['9940103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9941103:0:products = 100443 21']
genSeq.Pythia8B.Commands += ['9942103:0:products = 100443 21']

# Turn all decays off, then allow decay number 44 for Psi(2S)
# Psi(2S) -> Jpsi pi+ pi-
genSeq.Pythia8B.Commands += ['100443:onMode = off']
genSeq.Pythia8B.Commands += ['100443:44:onMode = on']

# Turn all decays off, then allow decay number 2 for Jpsi
# Jpsi -> mu+ mu-
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

# Only one open decay channel
# Set branching ratio to 1.0 
# To increase efficiency of event production
genSeq.Pythia8B.Commands += ['100443:44:bRatio = 1.0'] 
genSeq.Pythia8B.Commands += ['443:2:bRatio = 1.0']

# Select signal particles
genSeq.Pythia8B.SignalPDGCodes = [100443,443,-13,13,-211,211] #mu+ mu- pi- pi+
genSeq.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.38, 0.38]
genSeq.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [4.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]
