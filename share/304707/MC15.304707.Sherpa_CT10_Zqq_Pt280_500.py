include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa Z -> qq + 1,2,3,4j@LO with 280 GeV < ptV < 500 GeV."
evgenConfig.keywords = ["SM", "Z", "jets" ]
evgenConfig.contact  = [ "craig.sawyer@cern.ch", "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Zqq_Pt280_500"

evgenConfig.process="""
(run){
  %INIT_ONLY 1
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  QCUT:=20.;
}(run)

(processes){
  Process 93 93 -> 23[a] 93 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  Max_N_Quarks 6;
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.05 {5,6,7,8}
  End process
}(processes)

(selector){
  DecayMass 23 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 23 280.0 500.0
}(selector)
"""
