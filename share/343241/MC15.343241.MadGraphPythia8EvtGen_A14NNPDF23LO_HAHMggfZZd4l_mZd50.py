# Macro based on the one produced by Will Buttinger for the H->ZdZd->4l analysis
# Just changed to produce H->ZZd->4l decays and set parameter kappa as lowest as possible
# so that epsilon >> kappa. 


mass_points = ["15","20","25","30","35","40","45","50","55"] #mZd in GeV. maps onto 343234 - 343242


if runArgs.runNumber>=343234 and runArgs.runNumber<=343242:
   mzd = mass_points[runArgs.runNumber-343234];
   evgenConfig.description="MadGraph Hidden Abelian Higgs Model (HAHM): gg -> H -> ZZd -> 4l (l=e,mu) , with mZd=%sGeV" % mzd
   proc_card = """
import model HAHM_variableMW_v3_UFO
define l+ = e+ mu+
define l- = e- mu-
generate g g > h HIG=1 HIW=0 QED=0 QCD=0, (h > Z Zp, Z > l+ l-, Zp > l+ l-)"""
else:
   raise RuntimeError("Unrecognised runNumber: %d" % runArgs.runNumber)


proc_name = "HAHM_ggf_ZZd_4l_mZd%s" % mzd


#modifications to the param_card.dat (generated from the proc_card i.e. the specific model)
#if you want to see the resulting param_card, run Generate_tf with this jobo, and look at the param_card.dat in the cwd
#If you want to see the auto-calculated values of the decay widths, look at the one in <proc_name>/Cards/param_card.dat (again, after running a Generate_tf)
param_card_extras = { "HIDDEN": { 'epsilon': '1e-4', #kinetic mixing parameter
                                 'kap': '1e-10', #higgs mixing parameter
                                 'mzdinput': mzd, #Zd mass
                                 'mhsinput':'200.0' }, #dark higgs mass
                     "HIGGS": { 'mhinput':'125.0'}, #higgs mass
                     "DECAY": { 'wzp':'Auto', 'wh':'Auto', 'wt':'Auto' } #auto-calculate decay widths and BR of Zp, H, t
                  }


run_card_extras = { 'lhe_version':'2.0',
                   'cut_decays':'F',
                   'ptj':'0',
                   'ptb':'0',
                   'pta':'0',
                   'ptl':'0',
                   'etaj':'-1',
                   'etab':'-1',
                   'etaa':'-1',
                   'etal':'-1',
                   'drjj':'0',
                   'drbb':'0',
                   'drll':'0',
                   'draa':'0',
                   'drbj':'0',
                   'draj':'0',
                   'drjl':'0',
                   'drab':'0',
                   'drbl':'0',
                   'dral':'0' }



evgenConfig.keywords+=['exotic','BSMHiggs']
evgenConfig.contact = ['dparedes@cern.ch']
evgenConfig.process = "HAHM_H_ZZd_4l"

include("MC15JobOptions/MadGraphControl_Pythia8_A14_NNPDF23LO_EvtGen_Common.py")






evgenConfig.generators  = [ "MadGraph", "Pythia8", "EvtGen"] 
