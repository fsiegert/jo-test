model="LightVector"
mDM1 = 5.
mDM2 = 150.
mZp = 120.
mHD = 125.
widthZp = 5.729564e+01
widthN2 = 1.143965e+00
filteff = 4.484707e-02

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]
evgenConfig.minevents = 500

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
