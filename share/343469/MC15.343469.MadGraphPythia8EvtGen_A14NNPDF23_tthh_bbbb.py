from MadGraphControl.MadGraphUtils import *

mode=0

# setting higgs mass to 125GeV for param_card
higgsMass={'25':'1.250000e+02'} #MH


#---------------------------------------------------------------------------------------------------
# Setting parameters for run_card.dat
#---------------------------------------------------------------------------------------------------

extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',      
           'parton_shower':'PYTHIA8',
           'ptj':'0',
           'ptb':'0',
           'pta':'0',
           'ptjmax':'-1',
           'ptbmax':'-1',
           'ptamax':'-1',
           'etaj':'-1',
           'etab':'-1',
           'etaa':'-1',
           'etajmin':'0',
           'etabmin':'0',
           'etaamin':'0',
           'mmaa':'0',
           'mmaamax':'-1',
           'mmbb':'0',
           'mmbbmax':'-1',
           'drjj':'0',
           'drbb':'0',
           'draa':'0',
           'drbj':'0',
           'draj':'0',
           'drab':'0',
           'drjjmax':'-1',
           'drbbmax':'-1',
           'draamax':'-1',
           'drbjmax':'-1',
           'drajmax':'-1',
           'drabmax':'-1' }

fcard = open('proc_card_mg5.dat','w')
if (runArgs.runNumber == 343469):
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p  p > t t~ h h, ( h > b b~), (h > b b~)
    output -f""")
    fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")



# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events

safefactor = 1.1
nevents    = 10000*safefactor
if runArgs.maxEvents > 0:
        nevents=runArgs.maxEvents*safefactor


build_run_card(run_card_old=get_default_runcard(),run_card_new='Madgraph_run_card_ttHH.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#build_param_card(param_card_old='param_card.HeavyScalar.dat',param_card_new='param_card_new.dat',masses=higgsMass,extras=parameters)
build_param_card(param_card_old='Madgraph_param_card_ttHH.dat',param_card_new='Madgraph_param_card_ttHH_new.dat',masses=higgsMass)

    
print_cards()
    
runName='run_01'     


process_dir = new_process()
#generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)
generate(run_card_loc='Madgraph_run_card_ttHH.dat',param_card_loc='Madgraph_param_card_ttHH_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)


arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  

#### Ev Gen Configuration
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_tthh'
evgenConfig.keywords+=['Higgs', "hh"]
evgenConfig.contact = ['Alan Taylor <alan.james.taylor@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py") 
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=On"]

