include("MC15JobOptions/MadGraphControl_monoHiggs_zp2hdm.py")

evgenConfig.description = "Simplified Model of scalar mediator for\
MonoHiggs(h->bb) with mZ'="+str(mZp)+"GeV and mA0="+str(mA0)+"GeV"
evgenConfig.keywords = ["BSM", "BSMHiggs", "Higgs", "bbbar", "Zprime"]
evgenConfig.contact = ['Laser Kaplan <laser.seymour.kaplan@cern.ch>']

genSeq.Pythia8.Commands += [
    '25:oneChannel = on 1.0 100 5 -5 '
]
