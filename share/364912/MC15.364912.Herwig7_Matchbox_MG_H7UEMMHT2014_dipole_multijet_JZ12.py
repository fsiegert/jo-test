evgenConfig.description = 'Herwig7 NLO dijets with MMHT2014 and dipole shower, slice JZ 12'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']
evgenConfig.minevents = 50

include("MC15JobOptions/Herwig7EG_Matchbox_MG_H7UEMMHT2014_dipole_multijet_withGridpack.py")

include("MC15JobOptions/JetFilter_JZ12.py")
