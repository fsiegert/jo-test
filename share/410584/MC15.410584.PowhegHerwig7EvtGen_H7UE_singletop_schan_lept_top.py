#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top s-channel production (top), inclusive, with Powheg hdamp equal 1.5*top mass, ME NNPDF30 NLO, H7UE NNPDF23 LO'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel' ]
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch', 'ian.connelly@cern.ch' ]
evgenConfig.generators += [ 'Powheg' ]


#--------------------------------------------------------------
# Powheg single-top s-channel setup - V1
#--------------------------------------------------------------

evgenConfig.inputfilecheck = "410576"

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py')

# Bug-fix for incorrect gamma-fermion coupling
genSeq.Herwig7.Commands += [ "set /Herwig/Shower/GammatoQQbarSudakov:Alpha /Herwig/Shower/AlphaQED" ]

#-------------------------------------------------------------
# Filters
#-------------------------------------------------------------
