evgenConfig.description = "ND minimum bias, with the A2 MSTW2008LO tune (NO EvtGen), filtered on more then 160 tracks"

evgenConfig.keywords = ["QCD", "minBias", "SM"]

# Note: The tune used here (A2 MSTW2008LO) is not the standard one for high pT physics.  It is what we use for pile up at the start of run 2.  For standard high pT physics samples for MC15/run2 please see the A14 NNPDF23LO tune

include("MC15JobOptions/nonStandard/Pythia8_A2_MSTW2008LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]

include("MC15JobOptions/ChargedTrackFilter.py")
filtSeq.ChargedTracksFilter.NTracks = 160
# Filter efficiency 0.1%; 1000 events in  6h

evgenConfig.minevents = 1000



