#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.vdecaymode = 3   # tautau

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents *= 100 #
PowhegConfig.running_width = 1
PowhegConfig.mass_low = 10.
PowhegConfig.mass_high = 60.

#PowhegConfig.generate()
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')
#--------------------------------------------------------------
# FILTER
#--------------------------------------------------------------
include('MC15JobOptions/MultiElecMuTauFilter.py')
## Default cut params
filtSeq.MultiElecMuTauFilter.MinPt = 3500.
filtSeq.MultiElecMuTauFilter.MaxEta = 2.7
filtSeq.MultiElecMuTauFilter.MinVisPtHadTau = 10000.
filtSeq.MultiElecMuTauFilter.NLeptons = 2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
