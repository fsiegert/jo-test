model="InelasticVectorEFT"
mDM1 = 75.
mDM2 = 300.
mZp = 150.
mHD = 125.
widthZp = 5.968303e-01
widthN2 = 1.240646e-02
filteff = 8.635579e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
