model="LightVector"
mDM1 = 30.
mDM2 = 120.
mZp = 60.
mHD = 125.
widthZp = 2.387215e-01
widthN2 = 3.941759e+00
filteff = 4.363002e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
