# JO for Pythia 8 jet jet JZ2 slice with muon filter

evgenConfig.description = "Dijet truth jet slice JZ2, R04, with the A14 NNPDF23 LO tune with muon filter"
evgenConfig.keywords = ["QCD", "jets", "1muon", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

include("MC15JobOptions/JetFilter_JZ2R04.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 15.",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = "+str(minDict[1]) ,
                            "PhaseSpace:bias2SelectionPow = 5.0"
                            ]

include("MC15JobOptions/LowPtMuonFilter.py")

evgenConfig.minevents = 50
