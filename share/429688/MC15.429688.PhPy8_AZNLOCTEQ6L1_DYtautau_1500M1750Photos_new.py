#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
# duplicate JO to 429111, just PowhegConfig syntax modified (to be used in 19.2.4.10.2)
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.vdecaymode = 3   # tautau

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents *= 1.1 # increase number of generated events by 10%
PowhegConfig.running_width = 1
PowhegConfig.mass_low = 1500.
PowhegConfig.mass_high = 1750.

PowhegConfig.generate()
#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
#include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_AZNLO_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune and Photos++'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
