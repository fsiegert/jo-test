evgenConfig.description = "Single Pi- with flat eta-phi and fixed pT of 10 GeV"
evgenConfig.keywords = ["singleParticle", "pi-"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -211
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=10000, eta=[-4.2, 4.2])

