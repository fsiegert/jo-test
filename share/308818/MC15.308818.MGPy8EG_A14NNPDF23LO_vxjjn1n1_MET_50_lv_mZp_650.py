model="LightVector"
mDM1 = 5.
mDM2 = 680.
mZp = 650.
mHD = 125.
widthZp = 3.086608e+00
widthN2 = 2.564808e-01
filteff = 8.305648e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
