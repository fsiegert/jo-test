#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, three lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'Christian.Schmitt@cern.ch' ]

# afterburner contains a filter with about 2% efficiency
# The small number of events per job is because EvtInclusiveDecay has
# a memory leak when run in afterburner mode (will be fixed in
# release 20)
evgenConfig.minevents = 100
filterFactor = 50

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency in the afterburner
  PowhegConfig.nEvents     *= filterFactor
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()

  # Since our filter is in the afterburner phase, we need
  # to modify number of events in the generate phase to
  # make sure we don't run out
  hold = runArgs.maxEvents
  evgenConfig.minevents *= filterFactor
  runArgs.maxEvents = filterFactor*(hold-1)

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py') 


#  Run EvtGen as afterburner

include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
#evgenLog.info('3-lepton filter')

if runArgs.trfSubstepName == 'afterburn':
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
  filtSeq += MultiLeptonFilter('MultiLeptonFilter')
  myFilter = filtSeq.MultiLeptonFilter
  myFilter.NLeptons = 3
  myFilter.Ptcut = 10000
  myFilter.Etacut = 2.6

evgenConfig.generators  = [ "Powheg", "Pythia", "EvtGen"] 
