# W' -> WZ -> qqqq                                                                                                                                                                
# Wprime Mass (in GeV)                                                                                                                                                                                     
M_Wprime = float(runArgs.jobConfig[0].split('.')[-2].split('_')[-1].split('m')[1])
m_wz = float(runArgs.jobConfig[0].split('.')[-2].split('_')[-2].split('m')[1])

evgenConfig.contact = ["bnachman@cern.ch"]
evgenConfig.description = "Wprime->WZ->qqqq "+str(M_Wprime)+"GeV with NNPDF23LO PDF and A14 tune and m[W]="+str(m_wz)+", m[Z]="+str(m_wz)+", and Gamma[Z]=Gamma[W]=Gamma[W']=0.1 GeV"
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZ>qqqq"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]# Allow Wprime
genSeq.Pythia8.Commands += ["Wprime:coup2WZ = 1."]    # Wprime Coupling to WZ
genSeq.Pythia8.Commands += ["34:m0 = "+str(M_Wprime)] # Wprime mass
genSeq.Pythia8.Commands += ["34:doForceWidth=on"] #change the W' width 
genSeq.Pythia8.Commands += ["34:mWidth=0.1"]
genSeq.Pythia8.Commands += ["34:onMode = off"]# Turn off  decays
genSeq.Pythia8.Commands += ["34:onIfAll = 23 24"]# Turn on ->WZ
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all Z decays
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"]# Turn on hadronic Z
genSeq.Pythia8.Commands += ["23:onMode = off"]# Turn off all W decays
genSeq.Pythia8.Commands += ["23:onIfAny = 1 2 3 4 5"]# Turn on hadronic W
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on"]
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 5"]

genSeq.Pythia8.Commands += ["24:m0="+str(m_wz)] #change the W mass
genSeq.Pythia8.Commands += ["23:m0="+str(m_wz)] #change the Z mass
genSeq.Pythia8.Commands += ["24:doForceWidth=on"] #change the W width 
genSeq.Pythia8.Commands += ["24:mWidth=0.1"]
genSeq.Pythia8.Commands += ["WeakZ0:gmZmode = 2"]
genSeq.Pythia8.Commands += ["23:doForceWidth=on"] #change the Z width 
genSeq.Pythia8.Commands += ["23:mWidth=0.1"]
