evgenConfig.description = "Dijet truth jet slice JZ1A, with the A14 NNPDF23 LO tune and DstarMinus Filter"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ0W.py")
include("MC15JobOptions/DstarMinusFilter_minPt1GeV.py")

filtSeq.QCDTruthJetFilter.MinPt = 20*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 40*GeV

evgenConfig.minevents = 100

