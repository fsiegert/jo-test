evgenConfig.description = "Inelastic minimum bias, with the A2 MSTW2008LO tune (NO EvtGen)"

evgenConfig.keywords = ["QCD", "minBias", "SM"]

include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]
## include ("Charged_Tracks.py")

evgenConfig.minevents = 1000
