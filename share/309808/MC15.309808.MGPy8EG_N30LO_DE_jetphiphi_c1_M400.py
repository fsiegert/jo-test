# Inverse of M scale
invMscale=0.0025

# Wilson coefficients
c1=1.000000e+00
c2=0.000000e+00

evt_multiplier=1
filter_string="MET100"

include("MC15JobOptions/MadGraphControl_DarkEnergy_jetphiphi_c1_M400.py")
