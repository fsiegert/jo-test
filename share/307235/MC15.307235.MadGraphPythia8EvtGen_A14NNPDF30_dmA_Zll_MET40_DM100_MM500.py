Mchi =  100.
Mxi =  500.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 23.5714409591
evgenConfig.description = "Wimp pair monoZ with dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmA_Zll_MET40.py")
