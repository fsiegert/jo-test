#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
###include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
###PowhegConfig.mass_H  = 125.
###PowhegConfig.width_H = 0.00407

###PowhegConfig.complexpolescheme = 1 # use CPS

# Increase number of events requested to compensate for potential Pythia losses
###PowhegConfig.nEvents *= 2

#PowhegConfig.PDF = range(260000,260101) + range(90400,90433) + [11068] + [25200] + [13165]
#PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0]
#PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0]

#PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 11 11' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ee"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2electron", "mH125" ]
evgenConfig.contact     = [ 'r.turner@cern.ch' ]
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents = 5000
# required for local testing
###runArgs.inputGeneratorFile='group.phys-gener.powheg.344235.Powheg_NNPDF30_VBFH125_13TeV.TXT.mc15_v1/TXT.09954509._000001.tar.gz.1'
