# JO for Pythia 8 jet jet JZ1 slice

evgenConfig.description = "Dijet truth jet slice JZ1, with the A14 NNPDF23 LO tune and di-bjet filter"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

# Makes akt0.4 jets
include("MC15JobOptions/AntiKt4TruthJets_pileup.py")

from GeneratorFilters.GeneratorFiltersConf import DiBjetFilter
filtSeq += DiBjetFilter()
DiBjetFilter = filtSeq.DiBjetFilter
DiBjetFilter.LeadJetPtMin = 20000
DiBjetFilter.LeadJetPtMax = 60000
DiBjetFilter.DiJetPtMin = 0
DiBjetFilter.BottomPtMin = 5.0 * GeV
DiBjetFilter.BottomEtaMax = 3.0
DiBjetFilter.DiJetMassMin = 0
DiBjetFilter.DiJetMassMax = 300 * GeV
DiBjetFilter.JetPtMin = 15.0 * GeV
DiBjetFilter.JetEtaMax = 2.7
DiBjetFilter.DeltaRFromTruth = 0.3
# Use the jets that have been built
DiBjetFilter.TruthContainerName = "AntiKt4TruthJets"
DiBjetFilter.LightJetSuppressionFactor = 10
DiBjetFilter.AcceptSomeLightEvents = False

# Get some events out
evgenConfig.minevents = 100
