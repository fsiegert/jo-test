evgenConfig.description = "Signal B+->Y(4260)K+->Zc(->J/psi(mumu)pi)piK"
evgenConfig.keywords = ["bottom","exclusive","Jpsi","2muon"]
evgenConfig.contact = [ 'wesong@cern.ch' ]
evgenConfig.process = "pp->bb->B+>Y(4260)K>Zc(+>J/psi(mumu)pi)piK"
evgenConfig.minevents = 1000
 
include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
 
#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
 
genSeq.Pythia8B.NHadronizationLoops = 1
 
#
# Y(4260), 1-, with BESIII 17
#
genSeq.Pythia8B.Commands += ['453124:new = Y(4260) anti-Y(4260) 3 0 0 4.222 0.045 4.010 4.600 0.']
genSeq.Pythia8B.Commands += ['453124:addChannel = 1 0.5 0 463124 -211']
genSeq.Pythia8B.Commands += ['453124:addChannel = 1 0.5 0 -463124 211']
 
#
# Zc(3900), 1+, with BESIII 13
#
genSeq.Pythia8B.Commands += ['463124:new = Zc(3900)+ Zc(3900)- 3 3 0 3.899 0.046 3.700 4.009 0.']
genSeq.Pythia8B.Commands += ['463124:addChannel = 1 1. 0 443 211']
 
#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
 
#
# B+ decays:
#
genSeq.Pythia8B.Commands += ['521:addChannel = 2 1.0 0 453124 321']
 
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

