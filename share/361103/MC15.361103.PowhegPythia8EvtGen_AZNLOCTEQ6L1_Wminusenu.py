#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')

#--------------------------------------------------------------
# Set the decay mode, checking which syntax to use
#--------------------------------------------------------------
if hasattr(PowhegConfig, "idvecbos"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.idvecbos   = -24 # W-
    PowhegConfig.vdecaymode = 1  # enu
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "w- > e- ve"

# Configure Powheg setup
PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 1.2 # increase number of generated events by 20%
PowhegConfig.running_width = 1

PowhegControl.generate()
#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Wmin->enu production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan', 'electron', 'neutrino' ]
evgenConfig.minevents = 1000
