#--------------------------------------------------------------
# http://bbgen.web.cern.ch/bbgen/bruce/fluka_beam_gas_arc_4TeV/flukaIR15.html
# W. H. Bell <W.Bell@cern.ch>
#--------------------------------------------------------------

include( "MC15JobOptions/BeamHaloGenerator_Common.py")

# Default settings in common JO
genSeq.BeamHaloGeneratorAlg.inputType="FLUKA-RB"  
genSeq.BeamHaloGeneratorAlg.interfacePlane = -22600.0

# The probability of the event being flipped about the x-y plane.
genSeq.BeamHaloGeneratorAlg.flipProbability = 1.0
genSeq.BeamHaloGeneratorAlg.enableFlip = True

# The generator settings determine if the event is accepted.
#   * If the allowedPdgId list is not given all particles are accepted.
#   * Limits are in the form of (lower limit, upper limit)
#   * If a limit is not given it is disabled.
#   * If a limit value is -1 then it is disabled.
#   * All limits are checked against |value|
#   * r = sqrt(x^2 + y^2)
genSeq.BeamHaloGeneratorAlg.generatorSettings = [
  "shape cylinder", 
  "pzLimits 20.0",       # above 20 MeV
  "zLimits -22600. 22600.", # the length of the cavern 22.6m.
  "rLimits 0. 12500."]     # from the center of the detector, out to an  outer radius to include all pixel and first sct layer. 

#
evgenConfig.inputfilecheck = 'hllhc_ir1_b2_beamhalo_nprim7380000_30'
evgenConfig.description = "Non collision beam backgraund generator" 
evgenConfig.keywords = ["monojet" ] 
evgenConfig.minevents = 5000
