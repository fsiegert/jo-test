##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B_s -> (Psi2S->mumu)(Kstar->Kpi)
##############################################################
f = open("Bs_Psi2Smumu_KstarKpi_USER.DEC","w") #define file name
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Define Hp 0.685\n")
f.write("Define Hz 0.714\n")
f.write("Define Hm 0.143\n")
f.write("Define pHp 3.14\n")
f.write("Define pHz 0.0\n")
f.write("Define pHm -0.72\n")
f.write("Alias myPsi2S   psi(2S)\n")
f.write("Alias myKstar   anti-K*0\n")
f.write("Decay B_s0\n")
f.write("1.0000    myPsi2S  myKstar        PHSP;\n")
#f.write("1.0000     myPsi2S  myKstar        SVV_HELAMP Hp pHp Hz pHz Hm pHm;\n")
f.write("Enddecay\n")
f.write("Decay myPsi2S\n")
f.write("1.0000    mu+     mu-        VLL;\n")
f.write("Enddecay\n")
f.write("Decay myKstar\n")
f.write("1.0000    K+     pi-        VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################
##############################################################

evgenConfig.description = "Exclusive Bs->Psi2Smumu_KstarKpi production"
evgenConfig.keywords = ["exclusive","Bs","2muon"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")
###
###

include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 4

# Make Pythia select the events with Bs
genSeq.Pythia8B.SignalPDGCodes = [531]

genSeq.EvtInclusiveDecay.userDecayFile = "Bs_Psi2Smumu_KstarKpi_USER.DEC"

# Final state selections
filtSeq.BSignalFilter.B_PDGCode = 531
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 3500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6





