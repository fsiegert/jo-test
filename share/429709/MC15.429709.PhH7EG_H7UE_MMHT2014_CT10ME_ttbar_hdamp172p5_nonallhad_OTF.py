# based on the JobOptions MC15.429304

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox+Herwig7 ttbar production with Powheg hdamp equal top mass, H7-UE-MMHT tune, at least one lepton filter, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact        = ['paolo.francavilla@cern.ch', 'dominic.hirschbuehl@cern.ch', 'daniel.rauch@desy.de', 'james.robinson@cern.ch']

include('PowhegControl/PowhegControl_tt_Common.py')
# PowhegConfig.topdecaymode = 22222
PowhegConfig.decay_mode = "t t~ > all"
PowhegConfig.hdamp        = 172.5
# compensate filter efficiency
PowhegConfig.nEvents     *= 3.
# PowhegConfig.generateRunCard()
# PowhegConfig.generateEvents()
PowhegConfig.generate()

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

# NonAllHad filter
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(non-all had)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0
