evgenConfig.description = "Z->tau mu gamma production with A14_NNPDF23LO"
evgenConfig.keywords = ["BSM", "exotic", "Z"]
evgenConfig.contact = ["Wing Sheung Chan <ws.chan@cern.ch>"]
evgenConfig.process = "Ztaumugamma"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [#"WeakSingleBoson:ffbar2gmZ = on",     # create Z bosons
                            #"WeakZ0:gmZmode = 2",
                            "NewGaugeBoson:ffbar2gmZZprime = on", # create Z' bosons
                            "Zprime:gmZmode = 3",
                            "PhaseSpace:mHatMin = 60.",           # lower invariant mass
                            "32:onMode = off",                    # switch off all Z(') decays
                            "32:m0 = 91.1876",
                            "32:mWidth = 2.4952",
                            "32:doForceWidth = 1",
                            "32:oneChannel = 1 0.5 100 15 -13 22",  # Z(')->tau-mu+gamma
                            "32:addChannel = 1 0.5 100 -15 13 22"]  # Z(')->tau+mu-gamma

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 10000.
filtSeq.MultiLeptonFilter.Etacut = 2.8
filtSeq.MultiLeptonFilter.NLeptons = 1
