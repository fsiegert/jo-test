evgenConfig.description = "Single Pi0 with flat eta-phi and log E in [0.2, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "pi0"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(200., 2000000.), eta=[-5.5, 5.5])
