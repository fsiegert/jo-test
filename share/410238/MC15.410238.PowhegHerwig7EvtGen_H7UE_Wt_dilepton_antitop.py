#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 Wt production (antitop), dilepton, with CT10 for ME and MMHT2014 for PS, H7-UE-MMHT tune and with EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', '2lepton']
evgenConfig.contact     = [ 'cescobar@cern.ch','dominic.hirschbuehl@cern.ch']
evgenConfig.generators += [ 'Powheg', "Herwig7", "EvtGen" ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Wt_DR_Common.py')

PowhegConfig.topdecaymode = 11100 # inclusive W-from-top decays
PowhegConfig.wdecaymode = 11100 # inclusive W decays
PowhegConfig.ttype  = -1 # anti-top
# PowhegConfig.nEvents *= 3.
PowhegConfig.PDF     = 10800
PowhegConfig.mu_F    = 1.00000000000000000000
PowhegConfig.mu_R    = 1.00000000000000000000
PowhegConfig.generate()


#--------------------------------------------------------------
# Showering with Herwig7, H7-UE-MMHT tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10_LHEF_EvtGen_Common.py")
