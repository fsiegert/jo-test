from MadGraphControl.MadGraphUtils import *

evgenConfig.description = "MadGraph5+Pythia8 for 4top quarks from a pair of heavy resonances in 2UED-RPP with Mkk=1400 GeV"
evgenConfig.keywords+=['quark', 'exotic']
evgenConfig.inputfilecheck = '2UEDRPP1400GeV4Top'
evgenConfig.process = "4top quark production in 2UED-RPP with Mkk=1400 GeV"
evgenConfig.contact = ["Peyton Rose : prose@ucsc.edu"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
