#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal top mass, A14 tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 172.5
# compensate filter efficiency
PowhegConfig.nEvents     *= 100.
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# Configure the HT filter
include('MC15JobOptions/HTFilter.py')
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 600.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 1000.*GeV # Max HT to keep event
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT
