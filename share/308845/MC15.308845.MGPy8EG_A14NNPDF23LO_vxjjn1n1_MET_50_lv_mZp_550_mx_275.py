model="LightVector"
mDM1 = 275.
mDM2 = 1100.
mZp = 550.
mHD = 125.
widthZp = 2.346527e+00
widthN2 = 3.284799e+01
filteff = 9.823183e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
