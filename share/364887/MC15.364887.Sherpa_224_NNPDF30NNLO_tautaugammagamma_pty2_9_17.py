include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa tautaugammagamma + 0j@NLO + 1,2j@LO with 9<pT_y(sublead)<17."
evgenConfig.keywords = ["SM", "2tau", "2photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "heberth.torres@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_224_NNPDF30NNLO_tautaugammagamma_pty2_9_17"

genSeq.Sherpa_i.RunCard="""
(run){
  SHERPA_LDADD = SelectorsFixed

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=2; LJET:=4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % tau settings
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93 93 -> 22 22 15 -15 93{NJET}
  Order (*,4); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  PSI_ItMin 20000 {5}
  Integration_Error 0.99 {5}
  PSI_ItMin 50000 {6}
  Integration_Error 0.99 {6}
  End process
}(processes)

(selector){
  'PT'  22  9,E_CMS:9,17 [PT_UP]
  'DR'  22,90  0.3,1000:0.3,1000
  IsolationCut  22  0.1  2  0.10
  Mass  15  -15  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1" ]
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppllaa" ]
genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.ExtraFiles = [ "libSelectorsFixed.so" ]
