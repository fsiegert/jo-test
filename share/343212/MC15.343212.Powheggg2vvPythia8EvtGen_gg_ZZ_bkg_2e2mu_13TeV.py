#########################################################################
#
# gg2VV 3.1.6/Powheg/Pythia8 gg -> ZZ, with ZZ -> 2e2mu (background only)
#
# generator cuts on LHE level:
# m(ee) > 4GeV ; pt(ee) > 1GeV
# m(mumu) > 4GeV ; pt(mumu) > 1GeV
# m(eemumu) > 100 GeV
#

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'Powheg', 'gg2vv', 'Pythia8' ]
evgenConfig.description = 'gg2VV, ZZ-> 2e2mu using CT10 PDF, PowhegPythia8 , m(l+l-)>4GeV , pt(l+l-)>1GeV , m(4l)>100GeV , bkg only'
evgenConfig.keywords = ['diboson', '4lepton', 'electroweak', 'Higgs', 'ZZ']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.343212.gg_ZZ_bkg_2e2mu'

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

# boson decays already done in the lhe file
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

# no power shower, just wimpy showers
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 1' ]
