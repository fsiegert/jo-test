################################################################################
# Job options for Pythia8B_i generation of Bs->J/psi(mumu) K0S K0S X
################################################################################
evgenConfig.description = "Signal Bs->J/psi(mumu) K0S K0S X"
evgenConfig.keywords = ["bottom","exclusive","Bs","Jpsi","2muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->B_s->J/psi(mumu) K0S K0S X"
evgenConfig.minevents = 500

#include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.VetoDoubleBEvents = True

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2018
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2018
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
#

#
# chi_2c:
#
genSeq.Pythia8B.Commands += ['445:m0 = 3.55617']  # PDG 2018
genSeq.Pythia8B.Commands += ['445:mWidth = 0.00197'] # PDG 2018
#
genSeq.Pythia8B.Commands += ['445:1:bratio = 0.190'] # PDG 2018
genSeq.Pythia8B.Commands += ['445:24:bratio = 0.7407230']
genSeq.Pythia8B.Commands += ['445:addChannel = 1 0.00237 0 443 11 -11'] # PDG 2018
#
# chi_0c:
#
genSeq.Pythia8B.Commands += ['10441:m0 = 3.41471']  # PDG 2018
genSeq.Pythia8B.Commands += ['10441:mWidth = 0.0108'] # PDG 2018
#
genSeq.Pythia8B.Commands += ['10441:8:bratio = 0.014'] # PDG 2018
genSeq.Pythia8B.Commands += ['10441:24:bratio = 0.874466']
genSeq.Pythia8B.Commands += ['10441:addChannel = 1 0.000154 0 443 11 -11'] # PDG 2018
#
# h_1c:
#
genSeq.Pythia8B.Commands += ['10443:m0 = 3.52538']  # PDG 2018
genSeq.Pythia8B.Commands += ['10443:mWidth = 0.00070'] # PDG 2018
#
# chi_1c:
#
genSeq.Pythia8B.Commands += ['20443:m0 = 3.51067']  # PDG 2018
genSeq.Pythia8B.Commands += ['20443:mWidth = 0.00084'] # PDG 2018
#
genSeq.Pythia8B.Commands += ['20443:0:bratio = 0.343'] # PDG 2018
genSeq.Pythia8B.Commands += ['20443:22:bratio = 0.6203580']
genSeq.Pythia8B.Commands += ['20443:addChannel = 1 0.00365 0 443 11 -11'] # PDG 2018
#
# eta_c(2S):
#
# no decays to J/psi in Pyhia8 and PDG2018
#
# genSeq.Pythia8B.Commands += ['100441:m0 = 3.6376']  # PDG 2018
# genSeq.Pythia8B.Commands += ['100441:mWidth = 0.0113'] # PDG 2018
#
# psi(2S):
#
genSeq.Pythia8B.Commands += ['100443:m0 = 3.686097'] # PDG 2018
genSeq.Pythia8B.Commands += ['100443:mWidth = 0.000294'] # PDG 2018
#
genSeq.Pythia8B.Commands += ['100443:21:bratio = 0.001268'] # PDG 2018
genSeq.Pythia8B.Commands += ['100443:22:bratio = 0.0337'] # PDG 2018
genSeq.Pythia8B.Commands += ['100443:43:bratio = 0.1823'] # PDG 2018
genSeq.Pythia8B.Commands += ['100443:44:bratio = 0.3467'] # PDG 2018
genSeq.Pythia8B.Commands += ['100443:53:bratio = 0.098010']
#

#
# B_s:
#
genSeq.Pythia8B.Commands += ['531:m0 = 5.36689']  # PDG 2018
genSeq.Pythia8B.Commands += ['531:tau0 = 0.4524'] # PDG 2018
#
#
# B_s decays:
#
genSeq.Pythia8B.Commands += ['531:onMode = 3']
#
# channels 242-243 have onMode = 2 in Pythia8 default
#
genSeq.Pythia8B.Commands += ['531:242:onMode = 0']
genSeq.Pythia8B.Commands += ['531:243:onMode = 0']
#
# channels 244-245 have onMode = 3 in Pythia8 default
#
# genSeq.Pythia8B.Commands += ['531:244:onMode = 0']
# genSeq.Pythia8B.Commands += ['531:245:onMode = 0']
#

#
# Please note all J/psi mesons in each event decay to mu+mu- with Br == 1.
#
# Close direct and cascade H_b -> J/psi X  decays to avoid unphysical backgrounds
#
genSeq.Pythia8B.Commands += ['511:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['521:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['531:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['541:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['5122:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['5132:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['5232:offIfAny = 443 445 10441 10443 20443 100443']
genSeq.Pythia8B.Commands += ['5332:offIfAny = 443 445 10441 10443 20443 100443']
#
# Close also decays H_b -> c c-bar X producing J/psi in hadronisation
#
genSeq.Pythia8B.Commands += ['511:847:onMode = off']
genSeq.Pythia8B.Commands += ['511:849:onMode = off']
#
genSeq.Pythia8B.Commands += ['521:712:onMode = off']
genSeq.Pythia8B.Commands += ['521:714:onMode = off']
#
genSeq.Pythia8B.Commands += ['531:240:onMode = off']
#
genSeq.Pythia8B.Commands += ['5122:40:onMode = off']
#

#
# B0_s -> (cc-bar) K0S K0S : branchings as in Pythia8 for (cc-bar) K0 K0
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00070 0    443 310 310']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00016 0    445 310 310']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00003 0  10441 310 310']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00016 0  10443 310 310']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00026 0  20443 310 310']
#
# no decays to J/psi in Pyhia8 and PDG2018
#
# genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00012 0 100441 310 310']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00030 0 100443 310 310']
#
# B0_s -> (cc-bar) K0S K0S pi0 : branchings as in Pythia8 for (cc-bar) K0 K0 pi0
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00070 0    443 310 310 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00016 0    445 310 310 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00003 0  10441 310 310 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00016 0  10443 310 310 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00026 0  20443 310 310 111']
#
# no decays to J/psi in Pyhia8 and PDG2018
#
# genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00012 0 100441 310 310 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00030 0 100443 310 310 111']
#
# B0_s -> (cc-bar) K0S K0S pi0 pi0 : Br = 0.5 * Br(B0_s -> (cc-bar) K0S K0S pi0)
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000350 0    443 310 310 111 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000080 0    445 310 310 111 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000015 0  10441 310 310 111 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000080 0  10443 310 310 111 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000130 0  20443 310 310 111 111']
#
# no decays to J/psi in Pyhia8 and PDG2018
#
# genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000060 0 100441 310 310 111 111']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000150 0 100443 310 310 111 111']
#
#
# B0_s -> (cc-bar) K0S K0S pi+ pi- : Br = 0.5 * Br(B0_s -> (cc-bar) K0S K0S pi0)
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000350 0    443 310 310 211 -211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000080 0    445 310 310 211 -211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000015 0  10441 310 310 211 -211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000080 0  10443 310 310 211 -211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000130 0  20443 310 310 211 -211']
#
# no decays to J/psi in Pyhia8 and PDG2018
#
# genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000060 0 100441 310 310 211 -211']
#
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.000150 0 100443 310 310 211 -211']
#

#
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.Pythia8B.SignalPtCuts = [0.,3.5,3.5]
genSeq.Pythia8B.SignalEtaCuts = [100.,2.7,2.7]
#
genSeq.Pythia8B.NHadronizationLoops = 5

include("MC15JobOptions/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [531]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
#filtSeq.ParentChildwStatusFilter.PtMinParent =  0.0
#filtSeq.ParentChildwStatusFilter.EtaRangeParent = 1000.
filtSeq.ParentChildwStatusFilter.PDGChild = [310]
#filtSeq.ParentChildwStatusFilter.PtMinChild = 0.
#filtSeq.ParentChildwStatusFilter.EtaRangeChild = 1000.
