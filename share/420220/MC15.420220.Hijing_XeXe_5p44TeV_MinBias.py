###############################################################
#
# Job options file for Hijing generation of
# Xe + Xe collisions at 5440 GeV/(colliding nucleon pair)
#
# Andrzej Olszewski
#
# September 2017
#==============================================================

# use common fragment
include("MC15JobOptions/Hijing_Common.py")

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["minBias"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

evgenConfig.minevents = 1000

#----------------------
# Hijing Parameters
#----------------------
Hijing = genSeq.Hijing
#Hijing.McEventKey = "HIJING_EVENT"
#Hijing.McEventsRW = "HIJING_EVENT"
Hijing.Initialize = ["efrm 5440.", "frame CMS", "proj A", "targ A",
                    "iap 129", "izp 54", "iat 129", "izt 54",
# simulation of minimum-bias events
                    "bmin 0", "bmax 20",
# turns OFF jet quenching:
                    "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ... 
                    "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                    "ihpr2 21 1"]

if hasattr(fixSeq, "FixHepMC"):
    fixSeq.remove(FixHepMC())
