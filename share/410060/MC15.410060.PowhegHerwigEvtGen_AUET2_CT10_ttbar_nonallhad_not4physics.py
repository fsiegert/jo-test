#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Powheg + fHerwig/Jimmy  ttbar production with at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','onofrio@liverpool.ac.uk' ]

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 3.
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()

#--------------------------------------------------------------
# fHerwig showering
#--------------------------------------------------------------

  include("MC15JobOptions/nonStandard/Herwig_AUET2_CT10_Common.py")
  include("MC15JobOptions/nonStandard/Herwig_Powheg.py")
  include("MC15JobOptions/nonStandard/Herwig_Photos.py")
  include("MC15JobOptions/nonStandard/Herwig_Tauola.py")
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

## Run EvtGen in afterburner mode
include("MC15JobOptions/nonStandard/Herwig_EvtGen.py")

if runArgs.trfSubstepName == 'afterburn':
  evgenConfig.generators += ["Powheg"]
  genSeq.EvtInclusiveDecay.isfHerwig=True

evgenConfig.minevents = 2000
