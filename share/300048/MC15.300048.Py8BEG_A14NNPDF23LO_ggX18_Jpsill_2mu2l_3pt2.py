##############################################################
# Job options fragment for gg->X->Jpsill->mumu + ll (l=e/mu)  
# Created: 19 Aug 2016 by Tiesheng.Dai@cern.ch
##############################################################
evgenConfig.description = "PYTHIA8 gg->X->Jpsill->mumu + ll (l=e/mu) with A14 NNPDF23LO"
evgenConfig.keywords = ["heavyFlavour","Jpsi","4muon","2muon","2electron"]
evgenConfig.contact = [ 'Tiesheng.Dai@cern.ch' ]
evgenConfig.minevents = 5000
 
##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# J/psi -> mumu
##############################################################
f = open("MY_JPSI_USER_FORCED.DEC","w")
f.write("Decay J/psi\n")
f.write("1.0000    mu+  mu-             PHOTOS  VLL; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8B.Commands += ['Bottomonium:all = off',
                             'Higgs:useBSM = on',
                             'HiggsBSM:gg2H2 = on',
                             'HiggsH2:coup2d = 1.0',
                             'HiggsH2:coup2u = 1.0',
                             'HiggsH2:coup2Z = 0.0',
                             'HiggsH2:coup2W = 0.0',
                             'HiggsA3:coup2H2Z = 0.0',
                             'HiggsH2:coup2A3A3 = 0.0',
                             'HiggsH2:coup2H1H1 = 0.0',
                             '443:onMode = off',
                             '443:onIfMatch 13 13',
                             '35:mMin = 0',
                             '35:mMax = 25',
                             '35:m0   = 18.0',
                             '35:mWidth = 0.00',
                             '35:addChannel 1 0.50 100 11 -11 443',
                             '35:addChannel 1 0.50 100 13 -13 443',
                             '35:onMode = off',
                             '35:onIfMatch 11 -11 443', ## Jpsi ee
                             '35:onIfMatch 13 -13 443'  ## Jpsi mumu
                           ] 

genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.EvtInclusiveDecay.userDecayFile = "MY_JPSI_USER_FORCED.DEC"
#
### 3 lepton filter
#
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.NLeptons = 3
filtSeq.MultiLeptonFilter.Ptcut = 2000.0
filtSeq.MultiLeptonFilter.Etacut = 2.8
