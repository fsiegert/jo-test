evgenConfig.description = 'MadGraph5_aMC@NLO+Herwig++ bbH production'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', 'bbHiggs', 'diphoton' ]
evgenConfig.contact     = [ 'lorenz.hauswald@cern.ch', 'niklaos.rompotis@cern.ch', 'samina.jabbar@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHgamgam.py")
