###############################################################
# Pythia8 minimum bias (SD) samples
#==============================================================

evgenConfig.description = "SD Minbias with the default Donnachie-Landshoff model"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

# ... Main generator : Pythia8
include ("MC15JobOptions/nonStandard/Pythia8_Monash_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

# this uses the Donnachie-Landshoff model with default parameters
genSeq.Pythia8.Commands += ["Diffraction:PomFlux = 4"]
genSeq.Pythia8.Commands += ["Diffraction:PomFluxEpsilon = 0.085"]
genSeq.Pythia8.Commands += ["Diffraction:PomFluxAlphaPrime = 0.25"]

