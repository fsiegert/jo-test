# JO for Pythia 8 jet jet JZ1 slice with muon filter

evgenConfig.description = "Dijet truth jet slice JZ1, R04, with the A14 NNPDF23 LO tune with muon filter"
evgenConfig.keywords = ["QCD", "jets", "1muon", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ1R04.py")

filtSeq.QCDTruthJetFilter.MinPt = 20*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 40*GeV

include("MC15JobOptions/LowPtMuonFilter.py")

evgenConfig.minevents = 50
