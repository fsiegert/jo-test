import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=140
ihtmax=280
HTrange='midlowHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=20

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Znunu_Ht140_280_13TeV"
