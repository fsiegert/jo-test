
#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z->gamgamqqbar production
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

#PowhegConfig.runningscales = 1 # 
#PowhegConfig.vdecaymode = 10 # Z->inc
#PowhegConfig.hdecaymode = -1 #Pythia will handle h decays
#PowhegConfig.mass_Z_low = 10

#PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
#PowhegConfig.bornsuppfact = 0.00001

#PowhegConfig.withnegweights = 1 # allow neg. weighted events
#PowhegConfig.doublefsr = 1

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
#PowhegConfig.mass_H  = 125.
#PowhegConfig.width_H = 0.00407


#PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
#PowhegConfig.PDF = range(260000,260101) + range(90400,90433) + [11068] + [25200] + [13165]
#PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
#PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
#PowhegConfig.kappa_ghz = [1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0]
#PowhegConfig.kappa_ght = [1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0]
#PowhegConfig.kappa_ghb = [1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0]

#PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, gg->H+Z, H->ZZ->llll mh=125 GeV CPS"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ", "ZHiggs", "mH125" ]
evgenConfig.contact     = [ 'emountri@cern.ch' ]
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.process = "gg->ZH, H->ZZ4lep, Z->inc"
evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputconfcheck = "ggZH125_Zinc_NNPDF3"
evgenConfig.minevents = 2000



