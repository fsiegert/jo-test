mchi = 50
mphi = 1000
gx = 2.77696569326
filter_string = "T"
evt_multiplier = 25
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
