evgenConfig.description = "Single pi0 with flat eta-phi and fixed pT = 100 GeV"
evgenConfig.keywords = ["singleParticle", "pi0"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=100000.0, eta=[2.3, 4.3])

