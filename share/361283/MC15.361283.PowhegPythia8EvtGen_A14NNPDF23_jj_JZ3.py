#ktmin = { 1:2, 2:5, 3:15, 4:30, 5:75, 6:150, 7:250, 8:750, 9:1250 }
#supp  = { 1:60, 2:160, 3:400, 4:800, 5:1800, 6:3200, 7:5300, 8:9000, 9:11000 }

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["james.robinson@cern.ch"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_jj_Common.py")
PowhegConfig.bornktmin =15
PowhegConfig.bornsuppfact =400
PowhegConfig.nEvents = 12000
print PowhegConfig
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_Powheg.py")
include("MC15JobOptions/JetFilter_JZ3.py")
