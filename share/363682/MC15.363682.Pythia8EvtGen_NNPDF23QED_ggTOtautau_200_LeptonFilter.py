## Pythia8 photon induced di-lepton, gammagamma -> tautau

evgenConfig.description = "gammagamma -> tautau production with NNPDF23, 200<M GeV, central lepton filter pt>3.5 GeV"
evgenConfig.keywords = ["QCD", "2lepton", "exclusive", "dissociation", "diphoton"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]

include("MC15JobOptions/Pythia8_NNPDF23_NNLO_as118_QED_Common.py")

genSeq.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2tautau = on", # gg->tautau
    "PhaseSpace:mHatMin = 200.", # lower invariant mass
]


include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2
