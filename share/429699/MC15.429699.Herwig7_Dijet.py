from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

genSeq += Herwig7()

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "MMHT2014"
evgenConfig.description = "Herwig7 dijet sample with MMHT2014 PDF and corresponding tune"
evgenConfig.keywords    = ["SM","QCD", "dijet"]
evgenConfig.contact     = ["Daniel Rauch (daniel.rauch@desy.de)"]

## Configure Herwig/Matchbox
## These are the commands corresponding to what would go
## into the regular Herwig infile

generator = Hw7ConfigBuiltinME(genSeq, runArgs, run_name="HerwigBuiltinME")
generator.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
generator.shower_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
generator.tune_commands(ps_tune_name = "H7-PS-MMHT2014LO", ue_tune_name = "H7-UE-MMHT")

#Herwig7Config.tune_commands()

import os
if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

if int(verh7 == 0):
#   Herwig7Config.add_commands("""
# insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
   generator.add_commands("""
 insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
 set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
 set /Herwig/Cuts/JetKtCut:MinKT 15*GeV
 set /Herwig/Cuts/LeptonKtCut:MinKT 0.0*GeV
 """)
else:
#   Herwig7Config.add_commands("""
   generator.add_commands("""
## ------------------
## Hard process setup
## ------------------
 insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
 set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
 set /Herwig/Cuts/JetKtCut:MinKT 15*GeV
 set /Herwig/Cuts/LeptonKtCut:MinKT 0.0*GeV
 """)

## run the generator
generator.run()



