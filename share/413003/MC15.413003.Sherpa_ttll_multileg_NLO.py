include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")
evgenConfig.description = "Sherpa ttll+1j@NLO"
evgenConfig.keywords = ["ttZ", "SM", "multilepton"]
evgenConfig.contact = ["frank.siegert@cern.ch"]
evgenConfig.inputconfcheck = "ttll"
genSeq.Sherpa_i.RunCard="""
  (run){
  HARD_DECAYS=1;
  STABLE[6] = 0; WIDTH[6]=0.0;
  STABLE[23] = 0; STABLE[24] = 0;
  ACTIVE[25] = 0;
  
  CORE_SCALE VAR{H_T2/4};#;
  EXCLUSIVE_CLUSTER_MODE 1;
  
  #merging setup
  QCUT:=30.;
  LJET:=4,5; NJET:=1; 
  ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
  INTEGRATION_ERROR=0.05;
  
  # top/W decays
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
  }(run)
  
  (processes){
  Process 93 93 -> 6 -6 90 90 93{NJET};
  Order (*,2);
  CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET}; 
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator OpenLoops;
  End process;
  }(processes)
  (selector){
  Mass 11 -11 5 E_CMS;
  Mass 13 -13 5 E_CMS;
  Mass 15 -15 5 E_CMS;
  }(selector)
"""
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0"]
genSeq.Sherpa_i.NCores = 64
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplltt", "ppllttj"]

evgenConfig.minevents = 100
