model  = 'dmA'
mR     = 750
mDM    = 10000
gSM    = 0.20
gDM    = 1.00
widthR = 13.601690
phminpt= 100.000000
filteff = 0.141428

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")

evgenConfig.description = "DM axial Z'->jj, m_{R}=750 g_{SM}=0.20 m_{DM}=10000 g_{DM}=1.00"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = "p p > xi a, xi > j j"
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]
