##
## 27.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=27.0
gzpmul=3.200000e-02
include("MC15JobOptions/MGCtrl_Py8EG_NNPDF30nlo_Leptophilic_2muZp_4mu.py")
