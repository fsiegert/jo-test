include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "JHU+Pythia8, Graviton Longitudinal Bulk, mVV = 1000, @LO"
evgenConfig.keywords = ["BSM", "exotic", "spin2", "diboson", "jets" ]
evgenConfig.contact  = [ "sebastian.andres.olivares.pino@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.generators = ["JHU", "Pythia8", "EvtGen"]
evgenConfig.process = "gg -> G -> ZZ -> jet+jet"
evgenConfig.inputfilecheck = 'gg_GravitonLongitudinal_ZZ4q_m1000'
