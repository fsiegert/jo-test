
evgenConfig.process     = "VBF H->tautau->hh unpolarized"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = on', # decay of taus
                             '15:offIfAny = 11 13',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]
